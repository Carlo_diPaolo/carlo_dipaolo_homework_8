﻿using System;

namespace CarloFirstDungeon
{
		public class CarlosDungeon
		{
				public static string m_characterClass;
				public static string m_playerName;
				/// <summary>
				/// The entry point of the program, where the program control starts and ends.
				/// </summary>
				/// <param name="args">The command-line arguments.</param>
				public static void Main (string[] args)
				{
					Console.Title = "DISTRESS SIGNAL";
					Console.BackgroundColor = ConsoleColor.Black;
					Console.ForegroundColor = ConsoleColor.Green;
					Console.Clear();

					Console.WriteLine ("WELCOME TO MY FRIST DUNGEON!\n\n");

					Console.WriteLine("Answering a mysterious distress signal a small recon spaceship approaches a space station,\n" +
									  " there don't seem to be any signs of life coming from it.\n" +
									  "The ship's pilot initiates the docking procedure.\n\n");

					Choice_PlayerName ();

					Choice_CharacterClass ();

					Console.Clear ();
					Console.WriteLine (m_playerName + " the " + m_characterClass + " opens the airlock and steps onto the station.\n\n" +
									   "Press any Key to proceed.");

					Console.ReadKey();

					EntranceRoom.EnterRoom ();

				}

				public static void Choice_PlayerName()
				{
					Console.WriteLine ("DATA ARCHIVE CORRUPTED.\n\n" +
									   "plase enter your Name.\n\n");

				     m_playerName = Console.ReadLine();
				}

				public static void Choice_CharacterClass ()
				{
					Console.Clear();
					Console.WriteLine (m_playerName + ", in what type of field-work were you trained?\n\n"+
										"1-Soldier: I specialize in close-quarter ad ranged combat.\n" +
										"2-Engineer: I can repair and hack into most kinds of computers and security systems.\n" +
										"3-Biologist: I study and collect alien-life specimens.\n\n");

					ConsoleKey pressedKey = Console.ReadKey().Key;

					switch (pressedKey)
					{
						case(ConsoleKey.D1):
						{
							m_characterClass = "Soldier";
							break;
						}

						case(ConsoleKey.D2):
						{
							m_characterClass = "Engineer";
							break;
						}

						case(ConsoleKey.D3):
						{
							m_characterClass = "Biologist";
							break;
						}

						default:
						{
							Console.Clear();
							Console.WriteLine ("ERROR: The archive has no record of this specialization being taught at your training academy.\n" +
											   "please choose a valid specialization.");
							Choice_CharacterClass ();
							break;
						}

					}
				}
		}
}

