﻿using System;

namespace CarloFirstDungeon
{
	public class LostAndFound
	{
		public static void EnterRoom ()
		{
			Console.Clear();
			Console.WriteLine("Here is where all the lost items and forgotten luggage was stored, surely there must be something of value somewhere in here.");

			if(CarlosDungeon.m_characterClass == "Biologist")
			{
				Console.WriteLine("On the eastern side of the Room you see a door to the Security office\n" +
								  "1-Explore room.\n" +
								  "2-Go back to the Guest Welcome Center\n" +
								  "3-Go to the Security Office");
				ConsoleKey pressedkey = Console.ReadKey().Key;

				switch(pressedkey)
				{
					case(ConsoleKey.D1):
					{
						Console.Clear();
						Console.WriteLine("You search the Room for clues and useful objects.");
						break;
					}
					case(ConsoleKey.D2):
					{
						Console.Clear();
						EntranceRoom.EnterRoom();
						break;
					}
					case(ConsoleKey.D3):
					{
						Console.Clear();
						SecurityOffice.EnterRoom();
						break;
					}
					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						LostAndFound.EnterRoom();
						break;
					}
				}
			}
			else
			{
				Console.WriteLine("On the Northern side of the Room you see a door to the Guest Welcome Center\n\n" +
								  "1-Explore room.\n" +
								  "2-Go back to the Security Office\n" +
								  "3-Go to the Guest Welcome Center");
				ConsoleKey pressedkey = Console.ReadKey().Key;

				switch(pressedkey)
				{
					case(ConsoleKey.D1):
					{
						Console.Clear();
						Console.WriteLine("You search the Room for clues and useful objects.");
						break;
					}
					case(ConsoleKey.D2):
					{
						Console.Clear();
						SecurityOffice.EnterRoom();
						break;
					}
					case(ConsoleKey.D3):
					{
						Console.Clear();
						Console.WriteLine("The strange organic growth still blocks the path, you will have to take the long way around.\n\n" +
										  "Any Key-Go Back.");
						Console.ReadKey();
						VIPLounge.EnterRoom();
						break;
					}
					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						LostAndFound.EnterRoom();
						break;
					}
				}
			}
		}
	}
}

