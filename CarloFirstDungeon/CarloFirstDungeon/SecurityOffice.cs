﻿using System;

namespace CarloFirstDungeon
{
	public class SecurityOffice
	{
		public static void EnterRoom()
		{
			Console.Clear();
			Console.WriteLine("This was the command center for the station's security service.\n" +
							  "From here it woud be possible to monitor the whole area, if only the Console was powered up.\n" +
							  "Still, it's probably worth it to took around for anything that might be useful.");
						if(CarlosDungeon.m_characterClass == "Soldier")
						{
							SecOffSoldier();
						}
						else if(CarlosDungeon.m_characterClass == "Engineer")
						{
							SecOffEngineer();
						}
						else
						{
							SecOffBiologist();
						}
			

		}

		static void SecOffSoldier()
		{	
			Console.WriteLine("On the Eastern side of the room you see a door to the VIP Lounge, on the opposite side one to the Lost and found.\n\n" +
							  "1-Search the room.\n" +
							  "2-Go back to the Guest Welcome center.\n" +
							  "3-Go to the VIP Lounge.\n" +
							  "4-Go to the Lost and Found.");
			ConsoleKey pressedkey = Console.ReadKey().Key;

			switch(pressedkey)
			{
				case(ConsoleKey.D1):
				{
					Console.Clear();
					Console.WriteLine("You search the security office for clues and useful objects.");
					break;
				}
				case(ConsoleKey.D2):
				{
					
					Console.Clear();
					EntranceRoom.EnterRoom();
					break;
				}
				case(ConsoleKey.D3):
				{
					Console.Clear();
					VIPLounge.EnterRoom();
					break;
				}
				case(ConsoleKey.D4):
				{
					Console.Clear();
					LostAndFound.EnterRoom();
					break;
				}
				default:
				{
					Console.Clear();
					Console.WriteLine("PLEASE ENTER A VALID IMPUT");
					Console.ReadKey();
					Console.Clear();
					SecurityOffice.EnterRoom();
					break;
				}

			}
		}

		static void SecOffEngineer()
		{	
			Console.WriteLine("On the Northern side of the room you see a door to the Guest Welcome area, on the Western side one to the Lost and found.\n\n" +
							  "1-Search the room.\n" +
							  "2-Go back to the VIP Lounge.\n" +
							  "3-Go to the Guest Welcome Center\n" +
							  "4-Go to the Lost and Found");
			ConsoleKey pressedkey = Console.ReadKey().Key;

			switch(pressedkey)
			{
				case(ConsoleKey.D1):
				{
					Console.Clear();
					Console.WriteLine("You search the security office for clues and useful objects.");
					break;
				}
				case(ConsoleKey.D2):
				{
					Console.Clear();
					VIPLounge.EnterRoom();
					break;
				}
				case(ConsoleKey.D3):
				{
					Console.Clear();
					Console.WriteLine("Unfortunately, the door is still jammed, you will have to take the long way around.\n\n" +
									  "Any Key-Go Back.");
					Console.ReadKey();
					SecurityOffice.EnterRoom();
					break;
				}
				case(ConsoleKey.D4):
				{
					Console.Clear();
					LostAndFound.EnterRoom();
					break;
				}
				default:
				{
					Console.Clear();
					Console.WriteLine("PLEASE ENTER A VALID IMPUT");
					Console.ReadKey();
					Console.Clear();
					SecurityOffice.EnterRoom();
					break;
				}

			}
		}

		static void SecOffBiologist()
		{	
			Console.WriteLine("On the Northern side of the room you see a door to the Guest Welcome area, on the Eastern side one to the VIP Lounge.\n\n" +
							  "1-Search the room.\n" +
							  "2-Go back to the Lost and Found.\n" +
							  "3-Go to the Guest Welcome Center\n" +
							  "4-Go to the VIP Lounge");	
			ConsoleKey pressedkey = Console.ReadKey().Key;

			switch(pressedkey)
			{
				case(ConsoleKey.D1):
				{
					Console.Clear();
					Console.WriteLine("You search the security office for clues and useful objects.");
					break;
				}
				case(ConsoleKey.D2):
				{
					
					Console.Clear();
					LostAndFound.EnterRoom();
					break;
				}
				case(ConsoleKey.D3):
				{
					Console.Clear();
					Console.WriteLine("Unfortunately, the door is still jammed, you will have to take the long way around.\n\n" +
									  "Any Key-Go Back.");
					Console.ReadKey();
					SecurityOffice.EnterRoom();
					break;
				}
				case(ConsoleKey.D4):
				{
					Console.Clear();
					VIPLounge.EnterRoom();
					break;
				}
				default:
				{
					Console.Clear();
					Console.WriteLine("PLEASE ENTER A VALID IMPUT");
					Console.ReadKey();
					Console.Clear();
					SecurityOffice.EnterRoom();
					break;
				}

			}
		}
	}
}

