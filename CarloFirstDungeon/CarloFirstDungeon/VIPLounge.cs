﻿using System;

namespace CarloFirstDungeon
{
	public class VIPLounge
	{
		public static void EnterRoom()
		{
			Console.Clear();
			Console.WriteLine("This is the Area where the frist class passengers of the interplanetary shuttels could realax between flights.\n" +
							  "It seem completely empty now, but it might be worth it to take a look around.");

			

			if(CarlosDungeon.m_characterClass == "Engineer")
			{
				Console.WriteLine("On the Western side of the room you see a door to the Security Office\n\n" +
								  "1-Explore room.\n" +
								  "2-Go back to the Guest Welcome Center.\n" +
								  "3-Go to the Security Office.");

				ConsoleKey pressedkey = Console.ReadKey().Key;

				switch(pressedkey)
				{
					case(ConsoleKey.D1):
					{
						Console.Clear();
						Console.WriteLine("You search the Lounge for clues and useful objects.");
						break;
					}
					case(ConsoleKey.D2):
					{
						Console.Clear();
						EntranceRoom.EnterRoom();
						break;
					}
					case(ConsoleKey.D3):
					{
						Console.Clear();
						SecurityOffice.EnterRoom();
						break;
					}
					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						VIPLounge.EnterRoom();
						break;
					}
				}
			}

			else
			{

				Console.WriteLine("On the Northern side of the room you see a door to the Guest Welcome Center\n\n" +
								  "1-Explore room.\n" +
								  "2-Go back to the Security Office\n" +
								  "3-Go to the Guest Welcome Center");

				ConsoleKey pressedkey = Console.ReadKey().Key;

				switch(pressedkey)
				{
					case(ConsoleKey.D1):
					{
						Console.Clear();
						Console.WriteLine("You search the Lounge for clues and useful objects.");
						break;
					}
					case(ConsoleKey.D2):
					{
						Console.Clear();
						SecurityOffice.EnterRoom();
						break;
					}
					case(ConsoleKey.D3):
					{
						Console.Clear();
						Console.WriteLine("The Blast Doors are still sealed shut, you will have to take the long way around.\n\n" +
										  "Any Key-Go Back.");
						Console.ReadKey();
						VIPLounge.EnterRoom();
						break;
					}
					default:
					{
						Console.Clear();
						Console.WriteLine("PLEASE ENTER A VALID IMPUT");
						Console.ReadKey();
						Console.Clear();
						VIPLounge.EnterRoom();
						break;
					}
				}
			
			}
		}
	}
}

